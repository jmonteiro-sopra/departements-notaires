﻿<?php
// Fichiers de configuration générale.
require_once ('notaires_fonctions.php');
require_once ('lib/lib_recherche.php');


// Routage vers le bon contenu suivant l'action demandée
$action_admin   = getParam("admin");
$action_metier  = getParam("metier");
$action_index   = getParam("index");

// Si $float_content est renseigné, il sera affiché avant le gabarait principal
$float_content = null;

// on teste si le visiteur a soumis le formulaire de connexion
if (getPost('connexion') == 'Connexion') {
  $loginNotaire = trim(getPost('login'));
  $passNotaire = trim(getPost('pass'));
  if (! empty($loginNotaire) && ! empty($passNotaire) ) {
    list($err_code, $user_login, $user_email) = sessionLogin($loginNotaire, $passNotaire);
    if ( $err_code=="OK" || $err_code=="OK_FIRST") {
      $first_login = ($err_code=="OK_FIRST");
      // Redirection vers la page d'accueil en fonction du profil 
      $route = getWelcomePage(profileNotaire(), $first_login);
      header("Location: " .$route);
      exit();
    } 
    else if ( $err_code=="TOO_MANY_ERROR") {
      // Génère un message et lien pour déclencher l'envoi du mail donnant le code de déverrouillage
      $float_content = templateRender("sys/msg_verrouille.tpl", ["user_login"=>$user_login, "user_email"=>$user_email]);
    } 
    else if ( $err_code=="BAD_LOGIN") {
      $erreur = "L'identifiant ou le mot de passe est incorrect";
    } 
    else if ( $err_code=="ERR_SYS" ) {
      //
      $erreur = "### Erreur inattendue";
    }
  } else {
    $erreur = "Au moins un des champs est vide";
  }
}
// Sinon c'est peut être une demande de déconnexion
else if ( $action_index=="deconnexion" ) {
  sessionLogout();
  $action_index = "index"; // Affiche la page de login
}
// Initialise l'accès aux variables de session 
else {
  sessionUse();
}
// On vérifie que le délai d'inactivité n'est pas dépassé
if ( isLoggedIn() ) {
  sessionCheck();
}


// -----------
// Suivant le contexte, initialise le champ de saisie de l'identifiant de l'utilisateur
// -----------
$id_champ = "dever";
$valeur_champ = "Code CRPCEN";
$onload_function = "";
// formulaire réinit mot de passe
if (getParam("index")=="mail_reinit" ) {
  // valeur par défaut OK
}
// formulaire déverrouillage compte bloqué
else if (getParam("index")=="deverouille" ) {
  $valeur_champ = "Code reçu par mail";
}
// pas de traitement spécifique forward
else if (getParam("metier") || getParam("notaire") || getParam("admin")) {
  $id_champ = "";
}
// formulaire login initial
else {
  $id_champ = "login";
}
if ($id_champ != "") {
  $onload_function = "styleIdent('$id_champ', '$valeur_champ')";
}

// -----------
// Libellés de la première ligne
// -----------
// En haut à gauche : si loggé, affiche icône + libellé du l'utilisateur, sinon le numéro de version
$lib_utilisateur = $version_application;
if ( isLoggedIn() ) {
  $lib_utilisateur  =  "<a href='index.php?index=profil'>";
  $lib_utilisateur .=  "<img src=css/images/user.png width=16 height=16 />&nbsp;";
  // Si c'est une étude, préfixe avec "Etude :"
  if (isNotaire()) {
    $lib_utilisateur .= "Etude : ";
  }
  $lib_utilisateur .= getSession("libelleNotaire");
  $lib_utilisateur .= "</a>";
}
      
?>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo getPageTitle($nom_application); ?></title>
<link rel="stylesheet" href="css/notaires.css" type="text/css"    media="screen" />
<link rel="stylesheet" type="text/css" href="css/chosen.css" />
<link rel="icon" rel="shortcut icon" type="image/x-icon"   href="<?php echo $site_favicon; ?>" />
<script type="text/javascript" src="js/notaires.js"></script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/chosen.jquery.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
<script type="text/javascript">
	$(function() {
		$("#tablesortermember").tablesorter({sortList: [[1, 0]],emptyTo: 'bottom',widgets: ['zebra', 'columns']});
		$("#tablesorterliste").tablesorter({sortList: [[11, 1],[1, 0]],emptyTo: 'bottom',widgets: ['zebra', 'columns']});
		$("#tablesorter").tablesorter({sortList: [[11, 1]],emptyTo: 'bottom',widgets: ['zebra', 'columns']});
	});	
	</script>
</head>
<body  onload="<?php echo $onload_function; ?>" >

<?php 
if (!empty($float_content)) {
  echo $float_content;
}
?>

<div id="gabarit"><!-- gabarit -->

  <table id="bandeau_sup">
    <tr>
      <td align="left">
        <?php echo  $lib_utilisateur; ?>
      </td>
      <td>
        <?php if (isLoggedIn()) { ?>
            <a href="index.php?index=deconnexion">Déconnexion</a>
        <?php } ?>
        &nbsp;
      </td>
      <td align="right">
        <?php if (isLoggedIn()) { ?>
            <a href="index.php?index=cgu" target="_blank">Conditions Générales d'Utilisation</a>
        <?php } ?>
        &nbsp;
      </td>
      <td width="90" align="center">
        <a href="index.php?index=aide" target="_blank"><img src="css/images/aide.png" width="16" height="16" />&nbsp;Aide</a>
      </td>
    </tr>
  </table>
  
  <!--  Bandeau titre -->
  <table id="bandeau_inf">
    <tr>
      <td>
        <label id="titre"><?php echo $nom_application; ?></label>
      </td>
      <td align="right"><img class="logo" src="<?php echo $logo_appli; ?>" /></td>
    </tr>
  </table>

  <?php if (enMaintenance()) { ?>  
  <table id="bandeau_maint">
    <tr>
      <td>
        <label id="evt"><?php echo $message_maintenance; ?></label>
      </td>
    </tr>
  </table>
  <?php } ?>
  
    
<?php if (isLoggedIn()) { ?>
  <!--  Bandeau menu -->
  <table id=bandeau_menu>
    <tr>
      <?php if (isAdmin()){ ?>
      <td class="menu_item"><a class="menu" href="index.php?index=admin_accueil">Administration</a></td>
      <td class="menu_item"><a class=menu href="index.php?index=recherche">Recherche</a></td>
      <td class="menu_item"><a class=menu href="index.php?index=recherches">Listes de recherches</a></td>
      <td class="menu_item"><a class=menu href="index.php?metier=stats">Statistiques</a></td>
      
      <?php } else if (isNotaire()) { ?> 
      <td class="menu_item"><a class="menu" href="index.php?index=recherche">Nouvelle Recherche</a></td>
      <td class="menu_item"><a class="menu" href="index.php?index=recherches">Mes précédentes recherches</a></td>
      
      <?php } else if (isGestionnaire()) { ?>
      <td class="menu_item"><a class="menu" href="index.php?index=recherche">Recherche</a></td>
      <td class="menu_item"><a class="menu" href="index.php?index=recherches">Listes de recherches</a></td>
      <td class="menu_item"><a class="menu" href="index.php?metier=stats">Statistiques</a></td>
      <?php } ?>
    </tr>
  </table>
<?php } ?>

<div id="main">
<?php 
  if ( !empty($float_content) )  {
    // On ne fait rien de plus
  }
  // ACTION commune identifié/non identifié
  if ( $action_index=="aide" ) {
    echo templateRender("aide.tpl");
  } else {
    // MODE IDENTIFIE
    if ( isLoggedIn() ) {
      // Action réservée à l'admin
      if (isAdmin() && isset($action_admin) ) {
        include_once ("admin_$action_admin.php"); // TODO : migrer 
      }
      // Fix #45 l'accueil admin est réservé au profil admin
      else if ( isAdmin() && ($action_index=="admin_accueil") ) {
        echo templateRender("sys/admin_accueil.tpl");
      }
      // Action réservée admin ou métier
      else if ( (isAdmin() || isGestionnaire()) && isset($action_metier) ) {
        include_once ("metier_$action_metier.php"); // TODO : migrer 
      }
      // Recherche
      else if ( $action_index=="recherche" ) {
        $res_recherche = notairesSearch($_POST);
        if ( $res_recherche != "PARAM_VIDE" ) {
          echo templateRender("sys/reponse.tpl", ["message"=>$res_recherche]);
        } else {
          $params = [];
          if (hasDebug(2)) {
            include_once 'lib/lib_test.php';
            $params["test_data"] = getTestDataSearch();
          }
          echo templateRender("sys/frm_recherche.tpl", $params);
        }
      }
      // Mes recherches
      else if ( $action_index=="recherches" ) {
      	require_once ('lib/lib_listing.php');
      	$res_recherche = notairesListing($_POST);
  
      	$selecto= getPost('selecto');
      	$type_reponse= getPost('type_reponse');
      	$nom= getPost('nom');
      	$year_end= getPost('year_end');
      	$month_end= getPost('month_end');
      	$day_end= getPost('day_end');
      	$year_start= getPost('year_start');
      	$month_start= getPost('month_start');
      	$day_start= getPost('day_start');
      	$profil_notaire=getSession('profileNotaire');
      	
      	$search_listing= [
      	        "selecto"  		 => $selecto,
                "type_reponse"   => $type_reponse,
      			"nom"            => $nom,
      			"year_end"       => $year_end,
      			"month_end"		 => $month_end,
      			"day_end"		 => $day_end,
      			"year_start"	 => $year_start,
      			"month_start"	 => $month_start,
      			"day_start"		 => $day_start,
      			"message"        => $res_recherche,
      	    "mois_fr"        => tousLesMois(),
      			"liste_deroulante_individu" => $liste_deroulante_individu,
      			"liste_deroulante_etude" => $liste_deroulante_etude, 
            "annee_listing"  => $annee_listing,
      			"profil_notaire" => $profil_notaire
      	];
      	echo templateRender("sys/recherches.tpl",$search_listing); // TODO : migrer 
      }
      // Affichage info profil
      else if ( $action_index=="profil" ) {
        echo templateRender("sys/user_profile.tpl");
      }
      // Première connexion : changer le mot de passe
      else if ( $action_index=="first" ) {
        // Si le formulaire a été soumis, on vérifie les paramètres
        if ( getPost('inscription') ) {
          list($ok, $err_msg) = passChange(getpost("pass"), getPost("pass_confirm"), (getPost('cgu_accept') == 'true'));
          if ($ok) {
            echo templateRender("sys/msg_reinit_ok.tpl");
          } else {
            echo templateRender("sys/frm_index_first.tpl", ["error_msg"=>$err_msg]);
          }
        } else {
          echo templateRender("sys/frm_index_first.tpl");
        }
      }
      // Modif mot de passe (première fois ou suivante)
      else if ( $action_index=="modifier_mdp") {
        // Si le formulaire a été soumis, on vérifie les paramètres
        if ( getPost('pass_old') ) {
          list($ok, $err_msg) = passChange(getpost("pass"), getPost("pass_confirm"), true, getPost('pass_old'));
          if ($ok) {
            echo templateRender("sys/msg_reinit_ok.tpl");
          } else {
            echo templateRender("sys/frm_new_pass.tpl", ["error_msg"=>$err_msg]);
          }
        } else {
          echo templateRender("sys/frm_new_pass.tpl");
        }
      }
      // CGU
      else if ( $action_index=="cgu" ) {
        echo templateRender("cgu.tpl");
      }
      else if ($action_index="welcome") {
        echo templateRender(getWelcomeTemplate());
      }
      else {
        // On ne devrait jamais arriver ici
        echo templateRender("welcome.tpl");
      }
    }
    // MODE NON IDENTIFIE
    else {
      // Si un action rate, retour au formulaire d'identification avec un éventuel message
      $goBackToIndex = false;
      //logDebug("action_index=$action_index"); exit();
      switch ($action_index) {
        case "mail_deverouille":
          if (sendMailUnlock(getParam('login'), getParam('email'))) {
            // L'email a été envoyé, on affiche le formulaire de déverrouillage
            echo templateRender("sys/frm_deverouille.tpl");
          } else {
            $erreur = "Une erreur est survenue, veuillez réessayer";
            $goBackToIndex = true;
          }
          
          break;
        case "deverouille" :
          if (unlockAccount(trim(getPost("dever")))) {
            echo templateRender("sys/msg_deverouille_ok.tpl");
          } else {
            $erreur = "Le code ne correspond pas à celui qui vous a été envoyé.";
            echo templateRender("sys/frm_deverouille.tpl");
          }
          break;
          
        case "mail_reinit" :
          $user_login = getPost("user_login");
          $info_reinit="";
          if ( ! empty($user_login) ) {
            $info_reinit = sendMailReinitPass($user_login);
          }
          echo templateRender("sys/frm_mail_reinit.tpl", ["message"=>$info_reinit]);
          break;
          
        case "reinit" :
          // Le lien du mail utilise les arguments 'get'
          $user_id = getParam("id");
          $token = getParam("token");
          // Le formulaire utilise les arguments 'post'
          $pass_user_id = getPost("_id");
          $pass_token = getPost("_token");
          $pass = getPost("pass");
          $pass_confirm = getPost("pass_confirm");
          
          // C'est le lien de réinit qui est appelé depuis le mail
          if ( !empty($token) && !empty($user_id)  ) {
            // Affiche le formulaire si le token est bon
            if ( checkReinitPassToken($user_id, $token) ) {
              echo templateRender("sys/frm_reinit_pass.tpl", ["token"=>$token, "id"=>$user_id]);
            } else {
              echo templateRender("sys/msg_reinit_bad_link.tpl", ['diag'=>'1478']);
            }
          }
          // C'est le formulaire de reinit qui est soumis
          else if ( !empty($pass_token) && !empty($pass_user_id) && !empty($pass) && !empty($pass_confirm) ) {
            // Fix #43 Vérifie le token de nouveau
            if ( checkReinitPassToken($pass_user_id, $pass_token) ) {
              // Tente la modif
              list ($ok, $err_msg) = passReinit($pass_user_id, $pass, $pass_confirm);
              if ( $ok ) {
                echo templateRender("sys/msg_reinit_ok.tpl");
              } else {
                echo templateRender("sys/frm_reinit_pass.tpl", ["token"=>$pass_token, "id"=>$pass_user_id, "message"=>$err_msg]);
              }
            } else {
              echo templateRender("sys/msg_reinit_bad_link.tpl", ['diag'=>'2568']);
            }
          } else {
            echo templateRender("sys/msg_reinit_bad_link.tpl", ['diag'=>'3985']);
          }
          break;
          
        default :
          $goBackToIndex = true;
          
      }//switch
      // Message d'erreur éventuel
      if (isset($erreur)) {
        echo "<div id='message' align='center'>$erreur</div>";
      }
      if ($goBackToIndex) {
        // Formulaire d'identification
        echo templateRender("connexion.tpl");
      }
    }
  }
?>
  
  </div><!-- main -->    
</div><!--  gabarit -->

</body>
</html>