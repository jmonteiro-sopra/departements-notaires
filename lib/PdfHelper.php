<?php


use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * 
 * 
 *
 */
class PdfHelper
{
    private $pageCount = 0;
    
    private $htmlBuffer = "";
    
    private $resourcesPath;
    
    private $paperSize; // {@link Dompdf\Adapter\CPDF::$PAPER_SIZES}
    private $orientation; // 'portrait' ou 'landscape'

    /**
     * @param string $paperSize {@link Dompdf\Adapter\CPDF::$PAPER_SIZES}
     * @param string $orientation 'portrait' ou 'landscape'
     */
    public function __construct($paperSize = "A4", $orientation = "portrait")
    {
        $this->paperSize = $paperSize;
        $this->orientation = $orientation;
    }
    
    public function getPageCount()
    {
        return $this->pageCount;
    }
    
    /**
     * Définit une fois de début du document HTML 
     * @param string $resourcesPath chemin des fichiers images référencés dans le code HTML
     * @param string $cssFileName nom du fichier css
     */
    public function start($resourcesPath, $cssFileName)
    {
        $this->resourcesPath = $resourcesPath;
        
        $this->htmlBuffer = <<<_HEADER_
<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="$cssFileName">
<body>
_HEADER_;

    }
    
    /**
     * Ajoute du contenu qui doit tenir sur une page. A chaque nouvelle page, un saut de page HTML est généré 
     * automatiquement avant.
     * @param string $pageContent
     * @return int nombre pages après ajout
     */
    public function addPageContent($pageContent)
    {
        if ($this->pageCount > 0 )
        {
            // Ajoute saut de page avant
            $this->htmlBuffer .= '<div style="page-break-after: always;">&nbsp;</div>';
        }
        $this->htmlBuffer .= $pageContent;
        $this->pageCount++;
        
        return $this->pageCount;
    }
    
    public function end()
    {
        $this->htmlBuffer .= "</body></html>";
    }
    
    /**
     * Génère le document PDF dans le chemin spécifié
     * @param string $outputPath dossier où est créé le fichier
     * @param string $outputFileName nom du fichier, sans extension '.pdf' ajouté automatiquement
     * @return bool true en cas de succès, false sinon
     */
    public function outputToFile($outputPath, $outputFileName)
    {
        // Transforme le buffer HTML en PDF
        $dompdf= $this->render();
        
        // Récupère le contenu binaire et l'écrit dans un fichier
        $output = $dompdf->output(['compress'=>0]);
        $filePath = $outputPath . '/' . $outputFileName . '.pdf';
        $bytesWritten = file_put_contents($filePath, $output);
        
        return ($bytesWritten > 0);
    }
    
    /**
     * Renvoie le flux PDF
     * @param string $fileName nom du fichier téléchargé
     */
    public function outputToStream($fileName)
    {
        // Transforme le buffer HTML en PDF
        $dompdf= $this->render();
        
        // Renvoie le contenu binaire au navigateur
        $dompdf->stream( $fileName, ['compress'=>0]);
    }
    
    /**
     * @return Dompdf 
     */
    private function render()
    {
        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $dompdf = new Dompdf($options);
        $dompdf->setBasePath($this->resourcesPath);
        $dompdf->loadHtml($this->htmlBuffer);
        $dompdf->setPaper($this->paperSize, $this->orientation);
        $dompdf->render();
        return $dompdf;
    }
}

