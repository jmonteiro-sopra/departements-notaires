﻿<?php
/**
 * Génère le PDF des statistiques de recherche et connexion 
 */

require_once('notaires_fonctions.php');
require_once 'lib/PdfHelper.php';
require_once 'lib/GraphHelper.php';

sessionCheck();

if( !isAdmin() && !isGestionnaire() ){
    header ('Location: index.php');
    exit;
}

$year_start = getPost('year_start');
$month_start = getPost('month_start');
$year_end = getPost('year_end');
$month_end = getPost('month_end');

$dateFrom = new DateTime();
$dateFrom->setDate($year_start, $month_start, 1); // force au premier jour du mois
$dateFrom->setTime(0, 0); // force l'heure à 00:00

$dateTo = new DateTime();
$dateTo->setDate($year_end, $month_end, 1);
// force au dernier jour du mois exact, si on indique 31 en dur, on php décale au mois suivant en cas de débordement
$dateTo->modify('last day of this month'); 
$dateTo->setTime(23, 59); // force l'heure à 23:59


$statsRecherche = getStatsRecherche($dateFrom, $dateTo);

//logDebug("statsRecherche:" . var_dump($statsRecherche));
//exit;
$statsConnexion = getStatsConnexion($dateFrom, $dateTo);

// -----------------------------------------------------
// Génération de l'image
// -----------------------------------------------------
$graph = new GraphHelper();
$imgPath = "pdf/stats-" . session_id() . ".png";
//$graph->render($dateFrom, $dateTo, $statsRecherche, $statsConnexion);
$series = $graph->render($dateFrom, $dateTo, $statsRecherche, $statsConnexion, $imgPath);


// -----------------------------------------------------
// Génération du PDF
// -----------------------------------------------------
$template_params = array( "graph_path" => $imgPath, "series" => $series, "dateFrom" => $dateFrom, "dateTo" => $dateTo );
$pdf = new PdfHelper("A4", "landscape");
$pdf->start("./", ".private/lettre.css");
$pageContent = templateRender('pdf_statistiques.tpl', $template_params);
$pageContent = str_replace("{PAGE_COUNT}", $PAGE_COUNT, $pageContent);
$pdf->addPageContent($pageContent);

ob_end_clean();
$pdf->outputToStream("notaires-statistiques.pdf");

unlink($imgPath);

  
?>