{# Corps lettre réponse : personne inconnue #}
<p>Maître,</p>

<p>Vous questionnez le Département au sujet d'une éventuelle créance pour la personne suivante, {{sexe}} 
{{prenom}} {{nom}}, {{naissance}} le {{date_naissance}}, dont vous êtes en charge de la 
succession.</p>

<p>Je vous informe que la personne précitée n'a, à ma connaissance et sous réserve de l'exactitude des éléments que vous 
avez fournis pour la recherche, jamais bénéficié d'une aide récupérable du {{config.nom_departement_long}}.</p>
    
<p>Cette réponse reste soumise à l'exactitude des éléments renseignés et à l'existence d'une décision d'aide sociale 
au jour de la recherche.</p>
    
<p>Si une demande d'aide sociale est actuellement en cours d'instruction, le Département se réserve le droit de revenir 
vers vous une fois la décision d'accord prise, afin de faire valoir sa créance.</p> 

<p>Aussi, je vous invite à questionner vos clients à cet égard.</p>
    
<p>Je vous rappelle également que concernant les personnes dont le domicile de secours se situe sur le territoire de la 
Métropole, vous pouvez effectuer vos démarches par courrier électronique à l'adresse suivante, successions@grandlyon.com
</p>
    
<p> Je vous prie d'agréer, Maître, mes courtoises salutations.</p>

