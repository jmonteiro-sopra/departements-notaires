{# Message erreur dans le lien de réinit MDP #}


<div id="message_deverouille">
Il semble que ce lien ne soit pas correct, merci de réessayer :<br/>
- soit en retournant sur la page d'envoi du mail afin de regénérer un code en suivant ce lien : 
<a class="reinit" href="index.php?index=mail_reinit">Générer un nouveau code</a> 

<br/>

- soit en retournant sur la page d'accueil en suivant ce lien : <a class="reinit"  href="index.php">Retour à l'accueil</a>


<br/><br/><br/>
Code erreur = {{diag}}
</div>

