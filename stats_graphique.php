﻿<?php 
/**
 * Renvoie le flux image du graphique des statistiques de recherche/connexion pour les 5 derniers mois
 */
require_once 'notaires_fonctions.php';
require_once 'lib/GraphHelper.php';

sessionCheck();

if(isAdmin() || isGestionnaire())
{
    $dateTo = new DateTime();
    
    $dateFrom = new DateTime();
    $dateFrom->sub(new DateInterval("P4M")); // affiche au maximum 4 mois en arrière
    $dateFrom->setDate($dateFrom->format("Y"), $dateFrom->format("m"), 1); // force au premier jour du mois
    $dateFrom->setTime(0, 0); // force l'heure à 00:00
    
    $statsRecherche = getStatsRecherche($dateFrom, $dateTo);
    $statsConnexion = getStatsConnexion($dateFrom, $dateTo);
    
    // Pour le graphique affiché sur l'écran, on affiche d'abord le mois le plus récent
    $graph = new GraphHelper();
    $graph->render($dateTo, $dateFrom, $statsRecherche, $statsConnexion);
}
else
{
	header ('Location: index.php');
}
?>