{# Corps mail réponse : incertitude #}
<style>
  .signature{font-family:'verdana';font-size:75%;font-weight:bold;} 
  .corps{font-family:'verdana';font-size:80%;} 
  li{font-family:'verdana';font-size:80%;}
</style>
<p class="corps">Maître,<br/><br/>
Les éléments suivants ne permettent pas d'établir avec certitude l'identité de la personne. 
</p>
  <ul>
  	<li>Nom d'usage : {{nom}}</li>
  	<li>Nom d'état civil : {{nom_civil}}</li>
  	<li>Prénom 1 : {{prenom}}</li>
  	<li>Prénom 2 : {{prenomd}}</li>
  	<li>Date de naissance : {{date_naissance}}</li>
  </ul>
<p class="corps">
Les services du département ont pris en charge votre demande et vous répondront dans les meilleurs délais.
<br/>
Je vous prie d'agréer, Maître, mes courtoises salutations.</p>
<br/>
<p class="signature"> {{config.mail_signature | raw }}</p>


