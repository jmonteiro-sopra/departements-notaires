{# Formulaire de saisie nouveau mot de passe suite oubli #}

<div id="cadre_reinit">
  <form action="index.php?index=reinit" method="post">
    <table>
      <tr>
        <td colspan="2">
          <p id="message_reinit">
            Votre nouveau mot de passe doit être différent du précédent. Il doit contenir
            8 caractères avec au moins une majuscule, un chiffre et un caractère spécial (?/!*%$).
          </p>
        </td>
      </tr>
      <tr>
        <td><label id="ident">Mot de passe</label></td>
        <td align="right"><input type="password" class="input_connexion" name="pass" required="required"></td>
      </tr>
      <tr>
        <td><label id="ident">Confirmation du mot de passe</label></td>
        <td align="right"><input type="password" class="input_connexion" name="pass_confirm" required="required"></td>
      </tr>
      <tr>
        <td colspan="2" height="100px;"><input type="submit" id="submit_deverouille" name="inscription" value="Réinitialiser"></td>
      </tr>
    </table>
    <input type="hidden" id="_id" name="_id" value="{{id}}" />
    <input type="hidden" id="_token" name="_token" value="{{token}}" />
  </form>
</div>

<div id="message_r">{{message}}</div>